import Link from 'next/link';

export default function Home() {
  return (
    <div>
      <h1>Bem-vindos a mais uma Tech Talk!</h1>
      <Link href="/client-side">Exemplo CSR - client side rendering</Link>
      <br />
      <Link href="/server-side">Exemplo SSR - server side rendering</Link>
      <br />
      <Link href="/static-props">Exemplo SSG - static site generation</Link>
    </div>
  );
}
