import { UserServer } from '../components/UserServer.component';

export default function StaticPropsPage({ usuario }) {
  return <UserServer usuario={usuario} />;
}

export async function getStaticProps() {
  const response = await fetch('https://api.github.com/users/GustavoLarsen');
  const usuario = await response.json();

  if (!usuario) {
    return {
      notFound: true,
      revalidate: 5,
    };
  }

  return {
    props: {
      usuario,
    },
    revalidate: 3600, //revalidar em 1h == 3600 segundos
  };
}
