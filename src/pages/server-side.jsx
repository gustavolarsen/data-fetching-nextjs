import { UserServer } from '../components/UserServer.component';

export default function ServerSidePage({ usuario }) {
  return <UserServer usuario={usuario} />;
}

export async function getServerSideProps() {
  const response = await fetch('https://api.github.com/users/GustavoLarsen');
  const usuario = await response.json();

  if (!usuario) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      usuario,
    },
  };
}

export { ServerSidePage };
