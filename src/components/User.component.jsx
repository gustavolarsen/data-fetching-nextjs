import { useEffect, useState } from 'react';

const User = () => {
  const [usuario, setUsuario] = useState([]);

  useEffect(() => {
    const getUser = async () => {
      let response = await fetch('https://api.github.com/users/GustavoLarsen');
      response = await response.json();
      setUsuario(response);
    };

    getUser();
  }, []);

  return (
    <div>
      <p>
        <strong>Nome: </strong>
        {usuario.name}
      </p>
      <p>
        <strong>Localização: </strong>
        {usuario.location}
      </p>
      <p>
        <strong>Sobre: </strong>
        {usuario.bio}
      </p>
    </div>
  );
};

export { User };
