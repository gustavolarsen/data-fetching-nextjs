import { useEffect, useState } from 'react';

const UserServer = ({ usuario }) => {
  return (
    <div>
      <p>
        <strong>Nome: </strong>
        {usuario.name}
      </p>
      <p>
        <strong>Localização: </strong>
        {usuario.location}
      </p>
      <p>
        <strong>Sobre: </strong>
        {usuario.bio}
      </p>
    </div>
  );
};

export { UserServer };
