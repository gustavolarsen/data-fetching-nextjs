Este é um projeto em [Next.js](https://nextjs.org/) criado com [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

Após clonar o repositorio...

```bash
npm run dev
# or
yarn dev
```

Abra [http://localhost:3000](http://localhost:3000) no navegador para ver o resultado.

## About

Projeto criado para apresentar os conceitos de [`Data Feching`](https://nextjs.org/docs/basic-features/data-fetching/overview) com:

- (SSR) Server-side rendering
- (SSG) Static Site Generation
- (CSR) Client-side Rendering
